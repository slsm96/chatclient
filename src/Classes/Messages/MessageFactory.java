package Classes.Messages;

import java.util.ArrayList;

public class MessageFactory {

    private static final int TEXT_MESSAGE_LENGTH = 6;
    private static final int IMAGE_MESSAGE_LENGTH = 8;

    public static Message[] wrapMessages(String [] stringMessages){
        ArrayList<Message> messages = new ArrayList<Message>();
        for (String m:stringMessages) {
            messages.add(wrapMessage(m));
        }
        return messages.toArray(new Message[0]);
    }

    private static Message wrapMessage(String m) {
        String [] content = m.split("\\|");
        if (content.length == IMAGE_MESSAGE_LENGTH){return new ImageMessage(content);}
        if (content.length == TEXT_MESSAGE_LENGTH){return new TextMessage(content);}
        return corruptedMessage(content);
    }

    private static Message corruptedMessage(String [] content) {
        return new TextMessage(new String[]{"", "", "", "", "", "corrupted message: "+ String.join("|", content)});
    }

}

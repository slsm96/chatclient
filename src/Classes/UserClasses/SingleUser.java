package Classes.UserClasses;

import Classes.Messages.Outgoing.OutgoingMessage;

import java.io.IOException;

public class SingleUser extends User {

    public SingleUser(String name) {
        super(name);
    }

    @Override
    public void sendMessage(OutgoingMessage message) throws IOException {
        message.send(this.name);
    }


}

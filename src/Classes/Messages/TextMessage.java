package Classes.Messages;

public class TextMessage extends Message {


    private final String text;

    TextMessage(String[] s){
        super(s);
        text = s[5];
    }

    @Override
    public String toString() {
        return super.toString()+SEPERATOR+text;
    }

    public String getText() {
        return text;
    }
}

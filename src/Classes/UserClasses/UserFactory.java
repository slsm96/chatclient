package Classes.UserClasses;

import Classes.Exceptions.BackException;
import Classes.Exceptions.ElementNotFoundException;
import de.thm.oop.chat.base.server.BasicTHMChatServer;
import Classes.UserLogin;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

public class UserFactory {

    private static final ArrayList<GroupUser> allGroups = new ArrayList<>();
    private static final BasicTHMChatServer server = new BasicTHMChatServer();
    private static final String EXIT = " (type 0 to exit)";


    public static User selectUserType(UserLogin loginData) throws BackException {
        return switch (printAndScan("Send message to single user (type 1) or group user (type 2)" + EXIT + "?")) {
            case "1" -> selectSingleUser(loginData);
            case "2" -> selectGroupUser(loginData);
            default -> selectUserType(loginData);
        };
    }

    

    public static GroupUser returnIfGroupExists(String entry) throws ElementNotFoundException {
        for (GroupUser allGroup : allGroups) {
            if (Objects.equals(allGroup.name, entry)) {
                return allGroup;
            }
        }
        throw new ElementNotFoundException("Group not yet known!");

    }

    private static String printAndScan(String message) throws BackException {
        String input;
        Scanner sc = new Scanner(System.in);
        System.out.println(message);
        input = sc.nextLine();
        checkExit(input);
        return input;
    }
    public static void checkUsername(UserLogin loginData, String username) throws ElementNotFoundException, IllegalArgumentException, IOException {
		
		String[] allUsers = server.getUsers(loginData.getUsername(), loginData.getPassword());
        for (String allUser : allUsers) {
            if (Objects.equals(allUser, username)) {
                return;
            }
        }
		throw new ElementNotFoundException("No such user!");
		
		
	}
    
    private static SingleUser selectSingleUser(UserLogin loginData) throws BackException {
    	String username = printAndScan("Select a user to chat with" + EXIT + ":");
        try {
        	UserFactory.checkUsername(loginData, username);
        	return new SingleUser(username);
        }
        catch(ElementNotFoundException e){
        	System.out.println(e.getMessage());
        	return selectSingleUser(loginData);
        }
        catch(Exception e) {
        	throw new BackException();
        }
    }
    
    private static GroupUser selectGroupUser(UserLogin loginData) throws BackException{
    	String groupname = printAndScan("Select a Groupname" + EXIT + ":");
        try {
            return returnIfGroupExists(groupname);
        }
        catch(ElementNotFoundException e) {
            return new GroupUser(loginData, groupname);
        }
        
    }

    public static void addGroup(GroupUser gu){
        allGroups.add(gu);
    }

    public static String[] listGroupNames(){
        ArrayList<String> names = new ArrayList<>();
        if (allGroups.size() == 0){
            return new String[] {"No groups created"};
        }

        for (GroupUser g : allGroups) {
            names.add(g.name);
            for(String username:g.getUserList()){
                names.add("--"+username);
            }
        }

        return names.toArray(new String[names.size()]);
    }
    private static void checkExit(String input) throws BackException {
        if (input.equals("0")){
            throw new BackException();
        }
    }
}

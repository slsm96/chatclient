package Classes.Messages;

public class ImageMessage extends Message {

    private final String mimeType, size, url;

    public ImageMessage(String[] s){
        super(s);
        mimeType = s[5];
        size = s[6];
        url = s[7];
    }

    @Override
    public String toString() {
        return super.toString()+SEPERATOR+url;
    }

    public String getMimeType() {
        return mimeType;
    }

    public String getSize() {
        return size;
    }

    public String getUrl() {
        return url;
    }
}

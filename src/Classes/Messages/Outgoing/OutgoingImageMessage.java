package Classes.Messages.Outgoing;

import Classes.Exceptions.BackException;
import Classes.UserLogin;
import de.thm.oop.chat.base.server.BasicTHMChatServer;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class OutgoingImageMessage extends OutgoingMessage{

    private final String mimeType;
    private String path;
    private final InputStream imageData;

    public OutgoingImageMessage(UserLogin u, BasicTHMChatServer s) throws BackException {
        userLogin = u;
        server = s;
        imageData = readImageData();
        mimeType = readMimeType();
    }

    private String readMimeType() throws BackException {
        try {
            return Files.probeContentType(Path.of(path));
        }catch (IOException e){
            System.out.println("Unexpected error reading mime type");
            throw new BackException();
        }
    }

    private InputStream readImageData()throws BackException{
        try {
            path = getPath();
            File imageFile = new File(path);
            return new FileInputStream(imageFile);
        }catch (FileNotFoundException e){
            if(userWantsToRetry()){
                return readImageData();
            }else{
                throw new BackException();
            }
        }
    }

    private String getPath(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter file path:");
        return sc.nextLine();
    }

    private boolean userWantsToRetry() {
        Scanner sc = new Scanner(System.in);
        System.out.println("File not found, do you want to retry? [yes/no]");
        return switch (sc.nextLine().toUpperCase()) {
            case "YES", "Y" -> true;
            case "NO", "N" -> false;
            default -> userWantsToRetry();
        };
    }

    @Override
    public void send(String receiver) throws IOException {
        server.sendImageMessage(userLogin.getUsername(), userLogin.getPassword(), receiver, mimeType, imageData);
    }
}

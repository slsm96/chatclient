package Classes.Messages.Outgoing;

import Classes.UserLogin;
import de.thm.oop.chat.base.server.BasicTHMChatServer;

import java.io.IOException;
import java.util.Scanner;

public class OutgoingTextMessage extends OutgoingMessage {


    private final String text;

     public OutgoingTextMessage(UserLogin u, BasicTHMChatServer s) {
         userLogin = u;
         server = s;
         text = getText();
     }

     private String getText(){
         Scanner sc = new Scanner(System.in);
         System.out.println("Please enter message:");
         String s = sc.nextLine();
         return s;
     }

    @Override
    public void send(String receiver) throws IOException {
        server.sendTextMessage(userLogin.getUsername(), userLogin.getPassword(), receiver, text);
    }
}

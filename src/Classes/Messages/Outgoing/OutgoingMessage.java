package Classes.Messages.Outgoing;

import Classes.UserLogin;
import de.thm.oop.chat.base.server.BasicTHMChatServer;

import java.io.IOException;

public abstract class OutgoingMessage {
    protected UserLogin userLogin;
    protected BasicTHMChatServer server;
    public abstract void send(String receiver) throws IOException;

}

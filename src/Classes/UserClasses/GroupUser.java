package Classes.UserClasses;

import Classes.UserLogin;
import Classes.Exceptions.BackException;
import Classes.Exceptions.ElementNotFoundException;
import Classes.Messages.Outgoing.OutgoingMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

public class GroupUser extends User {
    private final ArrayList<String> userList = new ArrayList<>();


    public GroupUser(UserLogin loginData, String name) throws BackException {
        super(name);
        Scanner sc = new Scanner(System.in);
        System.out.println("Choose the first username to add to your broadcast:");
        String inputName = sc.nextLine();

        do {
            try {
                UserFactory.checkUsername(loginData, inputName);
                this.userList.add(inputName);
            }
            catch(ElementNotFoundException e) {
                System.out.println(e.getMessage());
            }
            catch(Exception e) {
            	throw new BackException();
            }
            finally {
                System.out.println("Select another user or type 0 to continue");
                inputName = sc.nextLine();
            }
        }while (!Objects.equals(inputName, "0"));
        UserFactory.addGroup(this);
    }

    public void sendMessage(OutgoingMessage message) throws IOException {
        for (String s : userList) {
            message.send(s);
        }
    }

    public ArrayList<String> getUserList() {
        return userList;
    }
}

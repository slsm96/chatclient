import Classes.Exceptions.BackException;
import Classes.Messages.Message;
import Classes.Messages.MessageFactory;
import Classes.Messages.Outgoing.OutgoingMessage;
import Classes.Messages.Outgoing.OutgoingMessageFactory;
import Classes.UserClasses.User;
import Classes.UserClasses.UserFactory;
import Classes.UserLogin;
import de.thm.oop.chat.base.server.BasicTHMChatServer;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    private static final BasicTHMChatServer server = new BasicTHMChatServer();

    public static void main(String[] args) throws IOException {
        UserLogin userLogin = login();
        String input;
        Scanner sc = new Scanner(System.in);

        while (true) {
            menu();
            input = sc.nextLine();

            switch (input) {
                case "1" -> sendMsg(userLogin);
                case "2" -> listMsgs(userLogin);
                case "3" -> listUsers(userLogin);
                case "4" -> listGroups();
                case "5" -> System.exit(1);
            }
        }
    }

    private static void listGroups() {
        for (String name : UserFactory.listGroupNames()) {
            System.out.println(name);
        }
    }

    private static void listUsers(UserLogin userLogin) throws IOException {
        String[] allUsers = server.getUsers(userLogin.getUsername(), userLogin.getPassword());
        for (String s:allUsers) {
            System.out.println(s);
        }
    }

    private static void listMsgs(UserLogin userLogin) {
        try {
            Message[] allMessages = MessageFactory.wrapMessages(server.getMessages(userLogin.getUsername(), userLogin.getPassword(), 0));
            for (Message s:allMessages) {
                System.out.println(s.toString());
            }

        }catch (IOException e){e.printStackTrace();}
    }

    private static void sendMsg(UserLogin userLogin) {
        try {
            OutgoingMessage msg = OutgoingMessageFactory.createMessage(userLogin);

            User chatPartner = UserFactory.selectUserType(userLogin);
            chatPartner.saveSendMessage(msg);
            
        }catch (BackException ignored){}
        
    }

    private static void menu() {
        System.out.println("---------------------");
        System.out.println("1: Send new Message");
        System.out.println("2: Fetch Messages");
        System.out.println("3: List all Users");
        System.out.println("4: List all Groups");
        System.out.println("5: Exit");
    }

    //TODO: Clean up and validation Check outsourcing to Classes.UserLogin Class?
    private static UserLogin login(){
        Scanner sc = new Scanner(System.in);
        String username;
        String password;

        System.out.print("Please enter your Username:");
        username = sc.nextLine();

        System.out.print("Please enter your Password:");
        password = sc.nextLine();

        try {
            return new UserLogin(username, password);
        } catch (IOException e){
            System.out.println("Error. Wrong username or passwort. Please try again");
            return login();
        }
    }
}
package Classes.Messages.Outgoing;

import Classes.Exceptions.BackException;
import Classes.UserLogin;
import de.thm.oop.chat.base.server.BasicTHMChatServer;

import java.util.Scanner;

public class OutgoingMessageFactory {
    private static final String TEXT_MESSAGE = "1";
    private static final String IMAGE_MESSAGE = "2";
    private static final String BACK = "3";
    private static final BasicTHMChatServer server = new BasicTHMChatServer();

    public static OutgoingMessage createMessage(UserLogin u) throws BackException {
        Scanner sc = new Scanner(System.in);
        printMenu();

        //TODO: Choose message type & create message
        String s = sc.nextLine();
        return switch(s) {
            case TEXT_MESSAGE -> new OutgoingTextMessage(u, server);
            case IMAGE_MESSAGE -> new OutgoingImageMessage(u, server);
            case BACK -> throw new BackException();
            default -> createMessage(u);
        };
    }

    private static void printMenu(){
        System.out.println("---------------------");
        System.out.println(TEXT_MESSAGE + ": Send a text");
        System.out.println(IMAGE_MESSAGE + ": Send an image");
        System.out.println(BACK + ": Back");
    }

}

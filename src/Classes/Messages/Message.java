package Classes.Messages;

import java.util.Objects;

public abstract class Message {
    protected String id, timestamp, direction, chatPartner, messageType;

    protected  static final String SEPERATOR  = "||";
    protected  static final String ANSI_RESET  = "\u001B[0m";
    protected static final String ANSI_GREEN  = "\u001B[32m";

    Message(String[] s){
        id = s[0];
        timestamp = s[1];
        direction = s[2];
        chatPartner = s[3];
        messageType = s[4];
    }
    public String toString(){
        return timestamp+SEPERATOR+colorise(((Objects.equals(direction, "in"))?chatPartner+"->you":"you->"+chatPartner), ANSI_GREEN);
    }
     protected String colorise(String text, String ansi){
        return ansi+text+ANSI_RESET;
     }

    public String getId() {
        return id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getDirection() {
        return direction;
    }

    public String getChatPartner() {
        return chatPartner;
    }

    public String getMessageType() {
        return messageType;
    }
}

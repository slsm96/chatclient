package Classes;

import de.thm.oop.chat.base.server.BasicTHMChatServer;

import java.io.IOException;

public class UserLogin {
    private final String username;
    private final String password;

    public UserLogin(String username, String password) throws IOException {
        this.check(username, password);
        this.username = username;
        this.password = password;
    }

    private void check(String username, String password) throws IOException {
        BasicTHMChatServer server = new BasicTHMChatServer();
        server.getUsers(username, password);
        //server.sendTextMessage(this.username, this.password, this.username, "login_test_message");
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }
}

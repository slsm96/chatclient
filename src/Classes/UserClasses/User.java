package Classes.UserClasses;

import Classes.Messages.Outgoing.OutgoingMessage;

import java.io.IOException;

public abstract class User {

    protected final String name;

    public User(String name) {
        this.name = name;
    }
    public abstract void sendMessage(OutgoingMessage message) throws IOException;
    
    public void saveSendMessage(OutgoingMessage message) {
    	try {
    		this.sendMessage(message);
    	}
    	catch(IOException e) {
    		System.out.println(e.getMessage());
    	}
    }

}
